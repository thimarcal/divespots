package apps.thiago.divespots.ui;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import apps.thiago.divespots.R;
import apps.thiago.divespots.adapter.DiveSpotsListAdapter;
import apps.thiago.divespots.adapter.OnDiveSpotItemClickListener;
import apps.thiago.divespots.data.DiveSpot;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class DiveSpotsListFragment extends Fragment implements OnDiveSpotItemClickListener {

    @BindView(R.id.dive_spots_list_recyclerview)
    RecyclerView mDiveSpotsListRv;
    private OnFragmentInteractionListener mListener;

    private RecyclerView.LayoutManager layoutManager;
    private DiveSpotsListAdapter mAdapter;

    private List<DiveSpot> mDiveSpots;

    public DiveSpotsListFragment() {
        // Required empty public constructor
    }

    public void setDiveSpots(List<DiveSpot> diveSpots) {
        mDiveSpots = diveSpots;

        if (null != mAdapter) {
            mAdapter.setDiveSpots(mDiveSpots);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dive_spots_list, container, false);
        ButterKnife.bind(this, view);

        if (null != savedInstanceState) {
            mDiveSpots = savedInstanceState.getParcelableArrayList(getString(R.string.dive_spots_key));
        }

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mDiveSpotsListRv.setLayoutManager(layoutManager);

        mAdapter = new DiveSpotsListAdapter(getContext(), this);
        mAdapter.setDiveSpots(mDiveSpots);
        mDiveSpotsListRv.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClicked(String diveSpotPosition) {
        Bundle bundle = new Bundle();
        int position = Integer.parseInt(diveSpotPosition);
        bundle.putParcelable(getString(R.string.dive_spot_key), mDiveSpots.get(position));
        mListener.onFragmentInteraction(OnFragmentInteractionListener.VIEW_DIVE_SPOT_DETAILS, bundle);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(getString(R.string.dive_spots_key),
                new ArrayList<DiveSpot>(mDiveSpots));
        super.onSaveInstanceState(outState);
    }
}
