package apps.thiago.divespots.data;

/**
 * Created by thiagom on 2/15/18.
 */

public interface OnFirebaseDBListener {
    public void onFirebaseDBDataChanged();
}
