package apps.thiago.divespots.ui;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import apps.thiago.divespots.R;
import apps.thiago.divespots.adapter.DiveSpotImageAdapter;
import apps.thiago.divespots.data.DiveSpot;
import apps.thiago.divespots.data.FirebaseDBUtils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class DiveSpotDetailsFragment extends Fragment {
    @BindView(R.id.add_fab)
    FloatingActionButton addFab;

    @BindView(R.id.dive_spot_description)
    TextView mDescriptionTv;

    @BindView(R.id.mapView)
    MapView mMapView;

    @BindView(R.id.dive_spot_images_recyclerview)
    RecyclerView divespotImagesRv;

    private OnFragmentInteractionListener mListener;

    private DiveSpot mDiveSpot;
    private FirebaseDBUtils mDbUtils;
    private List<String> mImages;

    private DiveSpotImageAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public DiveSpotDetailsFragment() {
        // Required empty public constructor
    }

    public void setDiveSpot(DiveSpot diveSpot) {
        mDiveSpot = diveSpot;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dive_spot_details, container, false);
        ButterKnife.bind(this, view);

        mDbUtils = FirebaseDBUtils.getInstance(getContext());

        if (null != savedInstanceState) {
            mDiveSpot = savedInstanceState.getParcelable(getString(R.string.dive_spot_key));
        }
        mDescriptionTv.setText(mDiveSpot.getDescription());

        mMapView.onCreate(savedInstanceState);

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                LatLng position = new LatLng(mDiveSpot.getLatitude(),
                        mDiveSpot.getLongitude());
                CameraUpdate center=
                        CameraUpdateFactory.newLatLngZoom(position, 10);

                googleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_landmark))
                        .anchor(0.0f, 1.0f)
                        .position(position));
                googleMap.moveCamera(center);
            }
        });

        mImages = mDbUtils.getImages(mDiveSpot.getIdentifier());

        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mAdapter = new DiveSpotImageAdapter();
        mAdapter.setImages(mImages);

        divespotImagesRv.setAdapter(mAdapter);
        divespotImagesRv.setLayoutManager(mLayoutManager);

        return view;
    }

    public void updateImages() {
        mImages = mDbUtils.getImages(mDiveSpot.getIdentifier());
        mAdapter.setImages(mImages);
    }

    @OnClick(R.id.add_fab)
    public void onFabClick() {
        Bundle bundle = new Bundle();
        bundle.putString(getString(R.string.dive_spot_id_key), mDiveSpot.getIdentifier());
        bundle.putString(getString(R.string.dive_spot_name_key), mDiveSpot.getName());
        mListener.onFragmentInteraction(OnFragmentInteractionListener.ADD_PICTURE, bundle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(getString(R.string.dive_spot_key), mDiveSpot);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        mMapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
