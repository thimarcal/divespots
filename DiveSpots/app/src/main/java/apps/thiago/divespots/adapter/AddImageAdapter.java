package apps.thiago.divespots.adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import apps.thiago.divespots.R;

/**
 * Created by thiagom on 2/19/18.
 */

public class AddImageAdapter extends RecyclerView.Adapter<AddImageAdapter.AddImageViewHolder> {

    private List<String> mImages;
    private Context mContext;

    private OnImageClickListener mListener;

    public void setImages(List<String> images) {
        mImages = images;
        notifyDataSetChanged();
    }

    public void setOnImageClickListener(OnImageClickListener listener) {
        mListener = listener;
    }

    @Override
    public AddImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View addImageView = inflater.inflate(R.layout.add_image_item, parent, false);
        return new AddImageViewHolder(addImageView);
    }

    @Override
    public void onBindViewHolder(AddImageViewHolder holder, int position) {
        holder.addImageCv.setTag(position);
        if (!mImages.get(position).equals(mContext.getString(R.string.default_add_image))) {
            holder.addImageIv.setContentDescription(mContext.getString(R.string.acc_remove_image_button));

            File localFile = new File(mImages.get(position));
            // Load image using Glide or Picasso
            Glide.with(mContext.getApplicationContext())
                    .load(localFile)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.no_image))
                    .into(holder.addImageIv);
        } else {
            holder.addImageIv.setImageResource(R.drawable.ic_add);
            holder.addImageIv.setContentDescription(mContext.getString(R.string.acc_add_image_button));
        }

    }

    @Override
    public int getItemCount() {
        if (null != mImages) {
            return mImages.size();
        }
        return 0;
    }

    class AddImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.add_image_imageview)
        ImageView addImageIv;
        @BindView(R.id.add_image_cardview)
        CardView addImageCv;

        public AddImageViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = Integer.parseInt(addImageCv.getTag().toString());

            if (mImages.get(position).equals(mContext.getString(R.string.default_add_image))) {
                // Add Image
                if (null != mListener) {
                    mListener.onItemClicked(OnImageClickListener.ADD_IMAGE, 0);
                }
            } else {
                if (null != mListener) {
                    mListener.onItemClicked(OnImageClickListener.REMOVE_IMAGE, position);
                }
            }
        }
    }
}
