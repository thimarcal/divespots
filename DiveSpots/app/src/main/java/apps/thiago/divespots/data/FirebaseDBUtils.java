package apps.thiago.divespots.data;

import android.content.Context;
import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import apps.thiago.divespots.R;

/**
 * Created by thiagom on 2/9/18.
 */

public class FirebaseDBUtils implements ValueEventListener {

    private static FirebaseDBUtils mInstance;
    private FirebaseDatabase mDbInstance;

    private List<OnFirebaseDBListener> mListeners = new ArrayList<>();

    private Context mContext;

    private DatabaseReference mDbReference;
    private DataSnapshot mDbSnapshot;

    private FirebaseDBUtils(Context context) {
        mDbInstance = FirebaseDatabase.getInstance();

        // Enables data Persistence to use data Offline
        mDbInstance.setPersistenceEnabled(true);
        mContext = context;

        mDbReference = mDbInstance.getReference();
        mDbReference.addValueEventListener(this);
    }

    /**
     * Singleton
     */
    public static FirebaseDBUtils getInstance(Context context) {
        if (null == mInstance) {
            mInstance = new FirebaseDBUtils(context);
        }

        return mInstance;
    }

    public boolean isDataAvailable() {
        return (mDbSnapshot != null);
    }

    /**
     * Adds a DB Listener, waiting for Data to be received
     *
     * @param listener
     */
    public void addOnFirebaseDBListener(OnFirebaseDBListener listener) {
        mListeners.add(listener);
    }

    /**
     * Method for retrieving the list of Countries where there are available dive spots
     *
     * @return list of countries
     */
    public List<String> getCountries() {
        if (null == mDbSnapshot) {
            // No data to be retrieved
            return null;
        }

        List<String> countries = new ArrayList<>();
        Iterator<DataSnapshot> children =
                mDbSnapshot.child(mContext.getString(R.string.firebase_dive_spots))
                        .getChildren().iterator();

        while (children.hasNext()) {
            countries.add(children.next().getKey());
        }

        return countries;
    }

    /**
     * Given a country, retrieves all the possible State/Regions for that Country
     *
     * @param country
     * @return list of states
     */
    public List<String> getStates(String country) {
        if (null == mDbSnapshot) {
            return null;
        }

        if (!mDbSnapshot.child(mContext.getString(R.string.firebase_dive_spots)).hasChild(country)) {
            return null;
        }

        List<String> states = new ArrayList<>();
        Iterator<DataSnapshot> children = mDbSnapshot
                .child(mContext.getString(R.string.firebase_dive_spots))
                .child(country).getChildren().iterator();

        while (children.hasNext()) {
            states.add(children.next().getKey());
        }

        return states;
    }

    /**
     * Given a Country / State pair, retrieve all cities that have Dive Spots
     * There can be the same city in different Country or states, like 'Sprgingfield', so
     * we're keeping the search using country + state
     *
     * @param country
     * @param state
     * @return List of cities
     */
    public List<String> getCities(String country, String state) {
        if (null == mDbSnapshot) {
            return null;
        }

        if (!mDbSnapshot.child(mContext.getString(R.string.firebase_dive_spots)).hasChild(country)
                || !mDbSnapshot.child(mContext.getString(R.string.firebase_dive_spots)).child(country)
                .hasChild(state)) {
            return null;
        }

        List<String> cities = new ArrayList<>();
        Iterator<DataSnapshot> children = mDbSnapshot
                .child(mContext.getString(R.string.firebase_dive_spots))
                .child(country)
                .child(state).getChildren().iterator();

        while (children.hasNext()) {
            cities.add(children.next().getKey());
        }

        return cities;
    }

    public List<DiveSpot> getDiveSpots(@NonNull String country, @NonNull String state, @NonNull String city) {
        if (null == mDbSnapshot) {
            return null;
        }
        if (!mDbSnapshot.child(mContext.getString(R.string.firebase_dive_spots)).hasChild(country)
                || !mDbSnapshot.child(mContext.getString(R.string.firebase_dive_spots)).child(country)
                .hasChild(state)
                || !mDbSnapshot.child(mContext.getString(R.string.firebase_dive_spots)).child(country)
                .child(state).hasChild(city)) {
            return null;
        }

        List<DiveSpot> diveSpots = new ArrayList<>();

        Iterator<DataSnapshot> children = mDbSnapshot
                .child(mContext.getString(R.string.firebase_dive_spots))
                .child(country)
                .child(state)
                .child(city).getChildren().iterator();


        while (children.hasNext()) {
            DiveSpot spot = new DiveSpot();
            DataSnapshot current = children.next();
            spot.setIdentifier(current.getKey());
            spot.setName(current.child(mContext.getString(R.string.firebase_spot_name))
                                .getValue().toString());
            spot.setDescription(current.child(mContext.getString(R.string.firebase_spot_description))
                    .getValue().toString());
            spot.setLatitude(Double.parseDouble(
                    current.child(mContext.getString(R.string.firebase_spot_latitude))
                    .getValue().toString()));
            spot.setLongitude(Double.parseDouble(
                    current.child(mContext.getString(R.string.firebase_spot_longitude))
                            .getValue().toString()));

            spot.setRating(Double.parseDouble(
                    current.child(mContext.getString(R.string.firebase_spot_rating))
                            .getValue().toString()));
            spot.setRatings(Long.parseLong(
                    current.child(mContext.getString(R.string.firebase_spot_ratings))
                            .getValue().toString()));
            Object imagePath = current.child(mContext.getString(R.string.firebase_profile_image))
                    .getValue();
            if (null != imagePath) {
                spot.setImagePath(imagePath.toString());
            }

            diveSpots.add(spot);
        }

        return diveSpots;
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        mDbSnapshot = dataSnapshot;

        for (OnFirebaseDBListener listener : mListeners) {
            listener.onFirebaseDBDataChanged();
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        // Failed to read value

    }

    /**
     * This method is used to add an imageUri to FirebaseDB
     * @param diveSpotId
     * @param imageId
     * @param imageUri
     */
    public void addPictureInfo(String diveSpotId, String diveSpotName, String imageId, String imageUri) {
        /*
         * Firebase paths cannot have '.', '#', '$', '[' or ']', so for avoiding this in Ids, we're
         * going to replace them for nothing
         */
        imageId = imageId.replace('.', '_');
        imageId = imageId.replace('#', '_');
        imageId = imageId.replace('$', '_');
        imageId = imageId.replace('[', '_');
        imageId = imageId.replace(']', '_');

        mDbInstance.getReference().child(mContext.getString(R.string.firebase_photos))
                    .child(diveSpotId)
                    .child(imageId)
                    .child(mContext.getString(R.string.firebase_photo_uri))
                    .setValue(imageUri);


        mDbInstance.getReference().child(mContext.getString(R.string.firebase_photos))
                .child(diveSpotId)
                .child(mContext.getString(R.string.firebase_spot_name))
                .setValue(diveSpotName).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });


    }

    public DatabaseReference addDiveSpot(String country, String state, String city, DiveSpot diveSpot) {
        DatabaseReference spotReference = mDbReference
                    .child(mContext.getString(R.string.firebase_dive_spots))
                    .child(country)
                    .child(state)
                    .child(city)
                    .child(diveSpot.getIdentifier());

        spotReference.child(mContext.getString(R.string.firebase_spot_name))
                .setValue(diveSpot.getName());
        spotReference.child(mContext.getString(R.string.firebase_spot_latitude))
                .setValue(diveSpot.getLatitude());
        spotReference.child(mContext.getString(R.string.firebase_spot_longitude))
                .setValue(diveSpot.getLongitude());
        spotReference.child(mContext.getString(R.string.firebase_spot_description))
                .setValue(diveSpot.getDescription());
        spotReference.child(mContext.getString(R.string.firebase_spot_rating))
                .setValue(0);
        spotReference.child(mContext.getString(R.string.firebase_spot_ratings))
                .setValue(0);

        return spotReference;
    }

    public void setProfileImage (DatabaseReference diveSpotReference, String imagePath) {
        diveSpotReference.child(mContext.getString(R.string.firebase_profile_image))
                         .setValue(imagePath);
    }

    /**
     * Retrieves a list of Images for a specific DiveSpot
     */
    public List<String> getImages(String diveSpotId) {
        List<String> images = new ArrayList<>();

        if (null == mDbSnapshot) {
            return null;
        }
        Iterator <DataSnapshot> iterator = mDbSnapshot.child(mContext.getString(R.string.firebase_photos))
                .child(diveSpotId)
                .getChildren().iterator();

        while (iterator.hasNext()) {
            DataSnapshot image = iterator.next();

            if (!image.getKey().equals(mContext.getString(R.string.firebase_spot_name))) {

                String path = image.child(mContext.getString(R.string.firebase_photo_uri)).getValue().toString();
                images.add(path);
            }
        }

        return images;
    }

    /**
     * Retrieves the list of All Images
     */
    public List<DiveSpotWidgetImageInfo> getAllImages() {
        List<DiveSpotWidgetImageInfo> images = new ArrayList<>();

        if (null == mDbSnapshot) {
            return null;
        }
        Iterator <DataSnapshot> spots = mDbSnapshot.child(mContext.getString(R.string.firebase_photos))
                                .getChildren().iterator();

        while (spots.hasNext()) {


            DataSnapshot spot = spots.next();

            Iterator<DataSnapshot> imageIterator = spot.getChildren().iterator();

            while (imageIterator.hasNext()) {
                DataSnapshot image = imageIterator.next();
                if (image.hasChild(mContext.getString(R.string.firebase_photo_uri)) &&
                        spot.hasChild(mContext.getString(R.string.firebase_spot_name))) {
                    String path = image.child(mContext.getString(R.string.firebase_photo_uri)).getValue().toString();
                    DiveSpotWidgetImageInfo info = new DiveSpotWidgetImageInfo();
                    info.setDiveSpotImage(path);
                    info.setDiveSpotName(spot.child(mContext.getString(R.string.firebase_spot_name))
                            .getValue().toString());

                    images.add(info);
                }
            }
        }

        return images;
    }
}
