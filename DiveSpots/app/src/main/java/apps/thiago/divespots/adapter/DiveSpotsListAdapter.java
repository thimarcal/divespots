package apps.thiago.divespots.adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import apps.thiago.divespots.R;
import apps.thiago.divespots.data.DiveSpot;

/**
 * Created by thiagom on 2/17/18.
 */

public class DiveSpotsListAdapter extends
            RecyclerView.Adapter<DiveSpotsListAdapter.DiveSpotsListViewHolder> {

    private List<DiveSpot> mDiveSpots = new ArrayList<>();

    private OnDiveSpotItemClickListener mListener;
    private Context mContext;

    public DiveSpotsListAdapter(Context context, OnDiveSpotItemClickListener listener) {
        mContext = context;
        mListener = listener;
    }

    @Override
    public DiveSpotsListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View spotView = inflater.inflate(R.layout.dive_spot_item, parent, false);
        return new DiveSpotsListViewHolder(spotView);
    }

    @Override
    public void onBindViewHolder(DiveSpotsListViewHolder holder, int position) {
        holder.diveSpotCardview.setTag(position);
        holder.diveSpotNameTv.setText(mDiveSpots.get(position).getName());

        String imagePath = mDiveSpots.get(position).getImagePath();
        if (null != imagePath && !imagePath.isEmpty()) {
            Glide.with(mContext.getApplicationContext())
                    .load(imagePath)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.no_image)
                    )
                    .into(holder.diveSportImage);
        }
    }

    @Override
    public int getItemCount() {
        return mDiveSpots.size();
    }

    public List<DiveSpot> getDiveSpots() {
        return mDiveSpots;
    }

    public void setDiveSpots(List<DiveSpot> diveSpots) {
        this.mDiveSpots = diveSpots;

        notifyDataSetChanged();
    }

    class DiveSpotsListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.dive_spot_name_textview)
        TextView diveSpotNameTv;
        @BindView(R.id.dive_spot_item_cardview)
        CardView diveSpotCardview;
        @BindView(R.id.dive_spot_item_imageview)
        ImageView diveSportImage;

        public DiveSpotsListViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            diveSpotCardview.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onItemClicked(v.getTag().toString());
        }
    }
}
