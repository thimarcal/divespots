package apps.thiago.divespots.ui;

import android.os.Bundle;

/**
 * Created by thiagom on 2/2/18.
 */

public interface OnFragmentInteractionListener {
    public static final int LOGIN_SUCCESS = 0;
    public static final int LOGOUT = 1;
    public static final int RESULTS_LOADED = 2;
    public static final int VIEW_DIVE_SPOT_DETAILS = 3;
    public static final int ADD_PICTURE = 4;
    public static final int IMAGE_ADDED = 5;

    public static final int ADD_IMAGE_RESPONSE = 6;
    public static final int DIVE_SPOT_ADDED = 7;

    public void onFragmentInteraction(int interaction, Bundle extras);
}
