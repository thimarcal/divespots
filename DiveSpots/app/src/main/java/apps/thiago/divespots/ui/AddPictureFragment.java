package apps.thiago.divespots.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import apps.thiago.divespots.R;
import apps.thiago.divespots.adapter.AddImageAdapter;
import apps.thiago.divespots.adapter.OnImageClickListener;
import apps.thiago.divespots.data.FirebaseStorageUtilsService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class AddPictureFragment extends Fragment implements OnImageClickListener {
    @BindView(R.id.add_images_recyclerview)
    RecyclerView addImagesRv;

    private static final int PICK_FROM_GALLERY = 0;

    private LayoutManager layoutManager;

    private AddImageAdapter addImageAdapter;

    private OnFragmentInteractionListener mListener;

    private String mDiveSpotId;
    private String mDiveSpotName;

    private List<String> images;

    public AddPictureFragment() {
        // Required empty public constructor
    }

    public void setDiveSpotId(String id) {
        mDiveSpotId = id;
    }
    public void setDiveSpotName(String name) {
        mDiveSpotName = name;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_picture, container, false);
        ButterKnife.bind(this, view);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            layoutManager = new GridLayoutManager(getContext(),
                    3, GridLayoutManager.VERTICAL, false);
        } else {
            layoutManager = new GridLayoutManager(getContext(),
                    5, GridLayoutManager.VERTICAL, false);
        }
        addImagesRv.setHasFixedSize(true);
        addImagesRv.setLayoutManager(layoutManager);

        addImageAdapter = new AddImageAdapter();
        if (null != savedInstanceState) {
            images = savedInstanceState.getStringArrayList(getString(R.string.selected_images_key));
            mDiveSpotId = savedInstanceState.getString(getString(R.string.dive_spot_id_key));
        } else {
            images = new ArrayList<>();
            images.add(getString(R.string.default_add_image));
        }
        addImageAdapter.setImages(images);
        addImageAdapter.setOnImageClickListener(this);
        addImagesRv.setAdapter(addImageAdapter);

        setRetainInstance(true);
        return view;
    }

    /**
     * This method adds the selected pictures to Firebase Storage and their links to FirebaseDB
     */
    @OnClick(R.id.add_pictures_button)
    public void addPictures() {
        if (images.size() == 1) {
            Snackbar.make(addImagesRv,
                          getString(R.string.no_images_selected), Snackbar.LENGTH_LONG)
                    .show();
            return;
        }
        // Remove the default image, that is the 'Add' button
        images.remove(getString(R.string.default_add_image));

        for (String picture : images) {
            Intent intent = new Intent(getContext(), FirebaseStorageUtilsService.class);
            intent.putExtra(getString(R.string.image_path_key), picture);
            intent.putExtra(getString(R.string.storage_path_key), getString(R.string.images_path));
            intent.putExtra(getString(R.string.dive_spot_id_key), mDiveSpotId);
            intent.putExtra(getString(R.string.dive_spot_name_key), mDiveSpotName);
            AddImageResultReceiver receiver = new AddImageResultReceiver(new Handler());
            receiver.setContext(getContext());
            receiver.setListener(mListener);
            intent.putExtra(getString(R.string.result_receiver_key), receiver);

            getContext().startService(intent);
        }

        mListener.onFragmentInteraction(OnFragmentInteractionListener.IMAGE_ADDED, null);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClicked(int action, int position) {
        if (action == ADD_IMAGE) {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            PackageManager pm = getActivity().getPackageManager();
            boolean handlerExists = galleryIntent.resolveActivity(pm) != null;

            // Start the Intent
            startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
        } else if (action == REMOVE_IMAGE) {
            images.remove(position);
            addImageAdapter.setImages(images);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*
         * Handle response from getting image from Gallery or any other app
         */
        if (requestCode == PICK_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri imageUri = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            // Get the cursor
            Cursor cursor = getActivity().getContentResolver().query(imageUri, filePathColumn,
                    null, null, null);
            // Move to first row
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String imgDecodableString = cursor.getString(columnIndex);
            cursor.close();

            images.add(imgDecodableString);
            addImageAdapter.setImages(images);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putStringArrayList(getString(R.string.selected_images_key),
                new ArrayList<String>(images));
        outState.putString(getString(R.string.dive_spot_id_key), mDiveSpotId);
        outState.putString(getString(R.string.dive_spot_name_key), mDiveSpotName);

        super.onSaveInstanceState(outState);
    }
}
