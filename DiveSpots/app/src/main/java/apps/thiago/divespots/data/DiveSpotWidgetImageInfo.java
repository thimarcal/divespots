package apps.thiago.divespots.data;

/**
 * Created by thiagom on 2/28/18.
 */

public class DiveSpotWidgetImageInfo {

    public String getDiveSpotName() {
        return diveSpotName;
    }

    public void setDiveSpotName(String diveSpotName) {
        this.diveSpotName = diveSpotName;
    }

    public String getDiveSpotImage() {
        return diveSpotImage;
    }

    public void setDiveSpotImage(String diveSpotImage) {
        this.diveSpotImage = diveSpotImage;
    }

    private String diveSpotName;
    private String diveSpotImage;
}
