package apps.thiago.divespots.adapter;

/**
 * Created by thiagom on 2/18/18.
 */

public interface OnDiveSpotItemClickListener {
    public void onItemClicked(String diveSpotId);
}
