package apps.thiago.divespots.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import apps.thiago.divespots.R;

/**
 * Created by thiagom on 2/26/18.
 */

public class DiveSpotImageAdapter extends
                RecyclerView.Adapter<DiveSpotImageAdapter.DiveSpotImageViewHolder> {


    private Context mContext;
    private List<String> mImages;

    public void setImages(List<String> images) {
        mImages = images;
        notifyDataSetChanged();
    }

    @Override
    public DiveSpotImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext().getApplicationContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);

        View view = inflater.inflate(R.layout.dive_spot_image_item, parent, false);

        return new DiveSpotImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DiveSpotImageViewHolder holder, int position) {
        if (null != mImages && null != mImages.get(position)) {
            Glide.with(mContext)
                    .load(mImages.get(position))
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.no_image))
                    .into(holder.diveSpotIv);
        }
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    public class DiveSpotImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.dive_spot_image_item_imageview)
        ImageView diveSpotIv;

        public DiveSpotImageViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
