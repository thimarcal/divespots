package apps.thiago.divespots;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.gms.maps.MapsInitializer;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import apps.thiago.divespots.data.DiveSpot;
import apps.thiago.divespots.data.FirebaseDBUtils;
import apps.thiago.divespots.data.OnFirebaseDBListener;
import apps.thiago.divespots.ui.AddDiveSpotFragment;
import apps.thiago.divespots.ui.AddPictureFragment;
import apps.thiago.divespots.ui.DiveSpotDetailsFragment;
import apps.thiago.divespots.ui.DiveSpotsListFragment;
import apps.thiago.divespots.ui.LoginFragment;
import apps.thiago.divespots.ui.OnFragmentInteractionListener;
import apps.thiago.divespots.ui.SearchFragment;
import apps.thiago.divespots.ui.SplashScreenFragment;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener,
                                                                    OnFirebaseDBListener {

    private static final int REQUEST_READ_STORAGE_PERMISSION = 0;
    private static final int REQUEST_ACCESS_COARSE_LOCATION = 1;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 2;

    @BindView(R.id.main_fragment_container)
    FrameLayout mainContainer;
    @BindView(R.id.big_fragment_container)
    @Nullable
    FrameLayout bigContainer;
    @BindView(R.id.full_screen_container)
    @Nullable
    ConstraintLayout fullScreenContainer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private Fragment mCurrentFragment;
    private Fragment mBigFragment;
    private FirebaseAuth mFirebaseAuth;

    private FirebaseDBUtils mDbUtils;

    private Snackbar snackbar;

    private List<DiveSpot> mDiveSpots;

    private int mCurrentDiveSpot;

    /*
     * Utility item positions (for back button)
     */
    private int mCountrySelection;
    private int mStateSelection;
    private int mCitySelection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        checkPermissions();

        mFirebaseAuth = FirebaseAuth.getInstance();

        MapsInitializer.initialize(this);
        setSupportActionBar(toolbar);

        if (null != savedInstanceState) {
            mCountrySelection = savedInstanceState.getInt(getString(R.string.selected_country_key));
            mStateSelection = savedInstanceState.getInt(getString(R.string.selected_state_key));
            mCitySelection = savedInstanceState.getInt(getString(R.string.selected_city_key));
        }

        if (null == fullScreenContainer) {
            if (null == savedInstanceState) {
                if (null != mFirebaseAuth.getCurrentUser()) {
                    mDbUtils = FirebaseDBUtils.getInstance(this);
                    mDbUtils.addOnFirebaseDBListener(this);

                    snackbar = Snackbar.make(mainContainer,
                            getString(R.string.loading_firebase), Snackbar.LENGTH_INDEFINITE);
                    snackbar.show();

                /*
                 * Not the best way to do that. Try to find a better way to identify data is loaded.
                 * Or maybe just change the method that is called (not the callback method).
                 */
                    if (mDbUtils.isDataAvailable()) {
                        onFirebaseDBDataChanged();
                    }
                } else {
                    mCurrentFragment = new LoginFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_fragment_container, mCurrentFragment).commit();
                }


            } else {
                mDbUtils = FirebaseDBUtils.getInstance(this);
                mDbUtils.addOnFirebaseDBListener(this);

                mCurrentFragment = getSupportFragmentManager().getFragment(savedInstanceState,
                        getString(R.string.fragment_key));
                mDiveSpots = savedInstanceState.getParcelableArrayList(getString(R.string.dive_spots_key));

                if (mCurrentFragment instanceof DiveSpotsListFragment) {
                    ((DiveSpotsListFragment) mCurrentFragment).setDiveSpots(mDiveSpots);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else if (mCurrentFragment instanceof DiveSpotDetailsFragment) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    mCurrentDiveSpot = savedInstanceState.getInt(
                            getString(R.string.current_dive_spot_key));
                    getSupportActionBar().setTitle(
                                mDiveSpots.get(mCurrentDiveSpot).getName());

                } else if (mCurrentFragment instanceof AddPictureFragment) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setTitle(getString(R.string.add_picture));
                } else if (mCurrentFragment instanceof AddDiveSpotFragment) {
                    getSupportActionBar().setTitle(R.string.dive_spot_add);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                }

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_fragment_container, mCurrentFragment).commit();
            }
        } else {
            // Tablet View
            mCurrentFragment = new LoginFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.full_screen_container, mCurrentFragment).commit();
        }

    }

    /*
     * This method handles click on Back in Action Bar. It just calls onBackPressed that has the
     * needed logic.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.add_divespot_menu:
                openAddDiveSpot();
                return true;

            case R.id.logout_menu_item:
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                mFirebaseAuth.signOut();
                mCurrentFragment = new LoginFragment();
                transaction.replace(R.id.main_fragment_container, mCurrentFragment);
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                transaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Displays the Fragment for Adding DiveSpot
     */
    public void openAddDiveSpot() {
        mCurrentFragment = new AddDiveSpotFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        getSupportActionBar().setTitle(R.string.dive_spot_add);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (null == fullScreenContainer) {
            transaction.replace(R.id.main_fragment_container, mCurrentFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    /*
     * Handles back press, that will pop previous fragment and set data if needed
     */
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
        } else {
            super.onBackPressed();
        }

        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (null == fragments || fragments.isEmpty()) {
            super.onBackPressed();
            return;
        }
        mCurrentFragment = fragments.get(0);

        // When returning, I need to update my fragment with Data, so checking its type allows me
        // to call the appropriate method
        if (mCurrentFragment instanceof SearchFragment) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle(R.string.app_name);

            ((SearchFragment) mCurrentFragment).setSelections(mCountrySelection,
                    mStateSelection,
                    mCitySelection);
        }

        if (mCurrentFragment instanceof DiveSpotsListFragment) {
            getSupportActionBar().setTitle(getString(R.string.app_name));
        }

        if (mCurrentFragment instanceof DiveSpotDetailsFragment) {
            getSupportActionBar().setTitle(
                    mDiveSpots.get(mCurrentDiveSpot).getName());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkConnection();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        if (null != menuInflater) {
            menuInflater.inflate(R.menu.main_menu, menu);
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onFragmentInteraction(int interaction, Bundle extras) {
        // Chooses which interaction happened
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (interaction) {
            case LOGIN_SUCCESS:
                mDbUtils = FirebaseDBUtils.getInstance(this);
                mDbUtils.addOnFirebaseDBListener(this);

                if (null == fullScreenContainer) {
                    if (mDbUtils.isDataAvailable()) {
                        mCurrentFragment = new SearchFragment();
                        transaction.replace(R.id.main_fragment_container, mCurrentFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        mCurrentFragment = new SplashScreenFragment();
                        transaction.replace(R.id.main_fragment_container, mCurrentFragment);
                        transaction.commit();
                        snackbar = Snackbar.make(mainContainer,
                                getString(R.string.loading_firebase), Snackbar.LENGTH_INDEFINITE);
                        snackbar.show();
                    }
                } else {
                    transaction.remove(mCurrentFragment);
                    if (mDbUtils.isDataAvailable()) {
                        mCurrentFragment = new SearchFragment();
                        mBigFragment = new SplashScreenFragment();
                        transaction.replace(R.id.main_fragment_container, mCurrentFragment);
                        transaction.replace(R.id.big_fragment_container, mBigFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    } else {
                        mCurrentFragment = new SplashScreenFragment();
                        transaction.replace(R.id.full_screen_container, mCurrentFragment);
                        transaction.commit();
                        snackbar = Snackbar.make(mainContainer, getString(R.string.loading_firebase), Snackbar.LENGTH_INDEFINITE);
                        snackbar.show();
                    }
                }
                break;

            case RESULTS_LOADED:
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                mCountrySelection = extras.getInt(getString(R.string.selected_country_key));
                mStateSelection = extras.getInt(getString(R.string.selected_state_key));
                mCitySelection = extras.getInt(getString(R.string.selected_city_key));
                mDiveSpots = extras.getParcelableArrayList(getString(R.string.dive_spots_key));
                if (null == fullScreenContainer) {
                    mCurrentFragment = new DiveSpotsListFragment();
                    ((DiveSpotsListFragment) mCurrentFragment).setDiveSpots(mDiveSpots);
                    transaction.replace(R.id.main_fragment_container, mCurrentFragment);

                } else {
                    mBigFragment = new DiveSpotsListFragment();
                    ((DiveSpotsListFragment) mBigFragment).setDiveSpots(mDiveSpots);
                    transaction.replace(R.id.big_fragment_container, mBigFragment);
                }
                transaction.addToBackStack(null);
                transaction.commit();
                break;

            case VIEW_DIVE_SPOT_DETAILS:
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                DiveSpot diveSpot = extras.getParcelable(getString(R.string.dive_spot_key));
                getSupportActionBar().setTitle(diveSpot.getName());

                if (null == fullScreenContainer) {
                    mCurrentFragment = new DiveSpotDetailsFragment();
                    ((DiveSpotDetailsFragment) mCurrentFragment).setDiveSpot(diveSpot);

                    transaction.replace(R.id.main_fragment_container, mCurrentFragment);
                } else {
                    mBigFragment = new DiveSpotDetailsFragment();
                    ((DiveSpotDetailsFragment) mBigFragment).setDiveSpot(diveSpot);

                    transaction.replace(R.id.big_fragment_container, mBigFragment);
                }
                transaction.addToBackStack(null);
                transaction.commit();
                break;

            case ADD_PICTURE:
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setTitle(getString(R.string.add_picture));
                if (null == fullScreenContainer) {
                    mCurrentFragment = new AddPictureFragment();
                    ((AddPictureFragment) mCurrentFragment).setDiveSpotId(
                            extras.getString(getString(R.string.dive_spot_id_key)));
                    ((AddPictureFragment) mCurrentFragment).setDiveSpotName(
                            extras.getString(getString(R.string.dive_spot_name_key)));

                    transaction.replace(R.id.main_fragment_container, mCurrentFragment);
                } else {
                    mBigFragment = new AddPictureFragment();
                    ((AddPictureFragment) mBigFragment).setDiveSpotId(
                            extras.getString(getString(R.string.dive_spot_id_key)));
                    ((AddPictureFragment) mBigFragment).setDiveSpotName(
                            extras.getString(getString(R.string.dive_spot_name_key)));


                    transaction.replace(R.id.big_fragment_container, mBigFragment);
                }
                transaction.addToBackStack(null);
                transaction.commit();
                break;

            case IMAGE_ADDED:
                onBackPressed();
                Snackbar.make(mainContainer,
                        getString(R.string.image_upload_notif), Snackbar.LENGTH_LONG)
                        .show();
                break;

            case ADD_IMAGE_RESPONSE:
                int resultCode = extras.getInt(getString(R.string.result_code_key));
                if (RESULT_OK == resultCode) {
                    String imageName = extras.getString(getString(R.string.images_path));
                    Snackbar.make(mainContainer, getString(R.string.image_upload_success,
                            imageName), Snackbar.LENGTH_SHORT).show();

                    String diveSpotId = extras.getString(getString(R.string.dive_spot_id_key));
                    String imageId = Base64.encode(imageName.getBytes(), Base64.DEFAULT).toString();
                    String imageUri = extras.getString(getString(R.string.image_uri_key));
                    String diveSpotName = extras.getString(getString(R.string.dive_spot_name_key));

                    mDbUtils.addPictureInfo(diveSpotId, diveSpotName, imageId, imageUri);

                } else if (RESULT_CANCELED == resultCode) {
                    String imageName = extras.getString(getString(R.string.images_path));
                    Snackbar.make(mainContainer, getString(R.string.image_upload_fail,
                            imageName), Snackbar.LENGTH_SHORT).show();
                }
                break;

            case DIVE_SPOT_ADDED:
                onBackPressed();
                break;
            default:
                break;
        }
    }

    @Override
    public void onFirebaseDBDataChanged() {
        // Check if user is already authenticated. If so, go to initial App screen, otherwise
        // display login screen
        if (null == fullScreenContainer) {
            if (null == mCurrentFragment || mCurrentFragment instanceof SplashScreenFragment) {
                if (null != mFirebaseAuth.getCurrentUser()) {
                    mCurrentFragment = new SearchFragment();
                } else {
                    mCurrentFragment = new LoginFragment();
                }
                if (null != snackbar) {
                    snackbar.dismiss();
                }
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.main_fragment_container, mCurrentFragment)
                        .commit();
            } else if (mCurrentFragment instanceof SearchFragment) {
                ((SearchFragment) mCurrentFragment).dataUpdated();
            } else if (mCurrentFragment instanceof DiveSpotDetailsFragment) {
                ((DiveSpotDetailsFragment)mCurrentFragment).updateImages();
            }

        } else {
            // Tablet View
            if (null == mCurrentFragment || mCurrentFragment instanceof SplashScreenFragment) {

                if (null != mCurrentFragment) {
                    getSupportFragmentManager().beginTransaction()
                            .remove(mCurrentFragment).commit();
                }
                if (null != mFirebaseAuth.getCurrentUser()) {
                    mCurrentFragment = new SearchFragment();
                    mBigFragment = new SplashScreenFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_fragment_container, mCurrentFragment)
                            .replace(R.id.big_fragment_container, mBigFragment)
                            .commit();
                } else {
                    mCurrentFragment = new LoginFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.full_screen_container, mCurrentFragment)
                            .commit();
                }
                if (null != snackbar) {
                    snackbar.dismiss();
                }

            } else {
                if (mCurrentFragment instanceof SearchFragment) {
                    ((SearchFragment) mCurrentFragment).dataUpdated();
                }
            }
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (null != mCurrentFragment) {
            getSupportFragmentManager().putFragment(outState,
                    getString(R.string.fragment_key),
                    mCurrentFragment);
        }

        outState.putInt(getString(R.string.current_dive_spot_key), mCurrentDiveSpot);
        outState.putParcelableArrayList(getString(R.string.dive_spots_key),
                (ArrayList<DiveSpot>) mDiveSpots);
        outState.putInt(getString(R.string.selected_country_key), mCountrySelection);
        outState.putInt(getString(R.string.selected_state_key), mStateSelection);
        outState.putInt(getString(R.string.selected_city_key), mCitySelection);

        super.onSaveInstanceState(outState);
    }

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_READ_STORAGE_PERMISSION);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        REQUEST_ACCESS_COARSE_LOCATION);
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_ACCESS_FINE_LOCATION);
            }
        }
    }

    private void checkConnection() {
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (!isConnected) {
            Snackbar.make(mainContainer,
                    getString(R.string.no_internet_connection),
                    Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Snackbar.make(mainContainer,
                        getString(R.string.storage_permission_warning), Snackbar.LENGTH_LONG).show();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


}