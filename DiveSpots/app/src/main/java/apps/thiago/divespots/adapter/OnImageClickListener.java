package apps.thiago.divespots.adapter;

/**
 * Created by thiagom on 2/19/18.
 */

public interface OnImageClickListener {
    public static final int ADD_IMAGE = 0;
    public static final int REMOVE_IMAGE = 1;

    public void onItemClicked(int action, int position);
}
