package apps.thiago.divespots.ui;

/**
 * Created by thiagom on 2/22/18.
 */

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import apps.thiago.divespots.R;

/**
 * ResultReceiver Class, for handling response from IntentService that puts images into
 * Firebase Storage
 */
public class AddImageResultReceiver extends ResultReceiver {
    private OnFragmentInteractionListener mListener;
    private Context mContext;

    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    public AddImageResultReceiver(Handler handler) {
        super(handler);
    }

    public void setContext(Context context) {
        mContext = context;
    }

    public void setListener(OnFragmentInteractionListener listener) {
        mListener = listener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);

        resultData.putInt(mContext.getString(R.string.result_code_key), resultCode);

        mListener.onFragmentInteraction(OnFragmentInteractionListener.ADD_IMAGE_RESPONSE, resultData);
    }

}