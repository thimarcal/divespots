package apps.thiago.divespots.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * This class represents a DiveSpot from Firebase Structure
 *
 * Created by thiagom on 2/14/18.
 */

public class DiveSpot implements Parcelable {

    private String identifier;
    private String name;
    private String description;
    private double latitude;
    private double longitude;
    private double rating;
    private double ratings;
    private String imagePath;

    public DiveSpot () {}

    public DiveSpot (Parcel parcel) {
        identifier = parcel.readString();
        name = parcel.readString();
        description = parcel.readString();
        latitude = parcel.readDouble();
        longitude = parcel.readDouble();
        rating = parcel.readDouble();
        ratings = parcel.readDouble();
        imagePath = parcel.readString();
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public double getRatings() {
        return ratings;
    }

    public void setRatings(double ratings) {
        this.ratings = ratings;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String path) {
        this.imagePath = path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identifier);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeDouble(rating);
        dest.writeDouble(ratings);
        dest.writeString(imagePath);
    }

    public static final Parcelable.Creator<DiveSpot> CREATOR = new Creator<DiveSpot>() {
        @Override
        public DiveSpot createFromParcel(Parcel in) {
            return new DiveSpot(in);
        }

        @Override
        public DiveSpot[] newArray(int size) {
            return new DiveSpot[size];
        }
    };
}
