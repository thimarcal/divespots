package apps.thiago.divespots.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import apps.thiago.divespots.R;
import apps.thiago.divespots.data.DiveSpot;
import apps.thiago.divespots.data.FirebaseDBUtils;
import apps.thiago.divespots.data.FirebaseStorageUtilsService;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class AddDiveSpotFragment extends Fragment {

    @BindView(R.id.add_dive_spot_container)
    ConstraintLayout rootLayout;
    @BindView(R.id.divespot_name_edittext)
    EditText mDiveSpotNameEt;
    @BindView(R.id.divespot_country_edittext)
    EditText mDiveSpotCountryEt;
    @BindView(R.id.divespot_state_edittext)
    EditText mDiveSpotStateEt;
    @BindView(R.id.divespot_city_edittext)
    EditText mDiveSpotCityEt;
    @BindView(R.id.latitude_edittext)
    EditText mDiveSpotLatitudeEt;
    @BindView(R.id.longitude_edittext)
    EditText mDiveSpotLongitudeEt;
    @BindView(R.id.divespot_description_edittext)
    EditText mDiveSpotDescriptionEt;
    @BindView(R.id.divespot_profile_imageview)
    ImageView mDiveSpotProfileIv;

    private static final int PICK_FROM_GALLERY = 0;

    private OnFragmentInteractionListener mListener;

    private String mSelectedProfileImage;

    private FirebaseDBUtils mDbutils = FirebaseDBUtils.getInstance(getContext());
    private DatabaseReference mAddedDiveSpot;

    private FusedLocationProviderClient mFusedLocationClient;

    Snackbar mSnackbar;

    public AddDiveSpotFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_dive_spot, container, false);

        ButterKnife.bind(this, view);

        if (null != savedInstanceState) {
            mDiveSpotNameEt.setText(savedInstanceState.getString(
                    getString(R.string.dive_spot_name_key)));
            mDiveSpotCountryEt.setText(savedInstanceState.getString(
                    getString(R.string.dive_spot_country_key)));
            mDiveSpotStateEt.setText(savedInstanceState.getString(
                    getString(R.string.dive_spot_state_key)));
            mDiveSpotCityEt.setText(savedInstanceState.getString(
                    getString(R.string.dive_spot_city_key)));
            mDiveSpotLatitudeEt.setText(savedInstanceState.getString(
                    getString(R.string.dive_spot_latitude_key)));
            mDiveSpotLongitudeEt.setText(savedInstanceState.getString(
                    getString(R.string.dive_spot_longitude_key)));
            mDiveSpotDescriptionEt.setText(savedInstanceState.getString(
                    getString(R.string.dive_spot_description_key)));

            mSelectedProfileImage = savedInstanceState.getString(getString(R.string.dive_spot_profile_image_key));
            if (null != mSelectedProfileImage && !mSelectedProfileImage.isEmpty()) {
                Glide.with(getContext().getApplicationContext())
                        .load(mSelectedProfileImage)
                        .apply(new RequestOptions()
                                .placeholder(R.drawable.no_image))
                        .into(mDiveSpotProfileIv);
            }
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.divespot_profile_imageview)
    public void selectProfileImage() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        PackageManager pm = getActivity().getPackageManager();
        boolean handlerExists = galleryIntent.resolveActivity(pm) != null;

        // Start the Intent
        startActivityForResult(galleryIntent, PICK_FROM_GALLERY);
    }

    @OnClick(R.id.add_divespot_button)
    public void addDiveSpot() {
        String name = mDiveSpotNameEt.getText().toString();
        String country = mDiveSpotCountryEt.getText().toString();
        String state = mDiveSpotStateEt.getText().toString();
        String city = mDiveSpotCityEt.getText().toString();
        String latitude = mDiveSpotLatitudeEt.getText().toString();
        String longitude = mDiveSpotLongitudeEt.getText().toString();
        String description = mDiveSpotDescriptionEt.getText().toString();

        if (name.isEmpty() || country.isEmpty() || state.isEmpty() || city.isEmpty()
                || latitude.isEmpty() || longitude.isEmpty() || description.isEmpty()
                || null == mSelectedProfileImage || mSelectedProfileImage.isEmpty()) {
            Snackbar.make(rootLayout, getString(R.string.missing_data_error), Snackbar.LENGTH_LONG).show();
            return;
        }

        DiveSpot diveSpot = new DiveSpot();
        /*
         * Add the DiveSpot to FirebaseDB, Add the profile image to Firebase Storage and when
         * path is received, update DB
         */
        String identifier = Base64.encode(name.getBytes(), Base64.DEFAULT).toString()
                .replace('.', '_').replace('#', '_')
                .replace('$', '_').replace('[', '_')
                .replace(']', '_');
        diveSpot.setIdentifier(identifier);
        diveSpot.setName(name);
        diveSpot.setLatitude(Double.parseDouble(latitude));
        diveSpot.setLongitude(Double.parseDouble(longitude));
        diveSpot.setDescription(description);

        mAddedDiveSpot = mDbutils.addDiveSpot(country, state, city, diveSpot);

        Intent intent = new Intent(getContext(), FirebaseStorageUtilsService.class);
        intent.putExtra(getString(R.string.image_path_key), mSelectedProfileImage);
        intent.putExtra(getString(R.string.storage_path_key), getString(R.string.profile_image_path));
        intent.putExtra(getString(R.string.dive_spot_id_key), diveSpot.getIdentifier());

        ProfileImageResultReceiver receiver = new ProfileImageResultReceiver(new Handler());
        receiver.setDiveSpotReference(mAddedDiveSpot);

        intent.putExtra(getString(R.string.result_receiver_key), receiver);
        mSnackbar = Snackbar.make(rootLayout, getString(R.string.adding_dive_spot), Snackbar.LENGTH_INDEFINITE);
        mSnackbar.show();
        getContext().startService(intent);
    }

    @OnClick(R.id.access_location_btn)
    public void getCurrentLocation() {

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            mSnackbar = Snackbar.make(rootLayout,
                    getString(R.string.get_current_location),
                    Snackbar.LENGTH_INDEFINITE);
            mSnackbar.show();
            mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    mSnackbar.dismiss();
                    if (null != location) {
                        mDiveSpotLatitudeEt.setText(String.valueOf(location.getLatitude()));
                        mDiveSpotLongitudeEt.setText(String.valueOf(location.getLongitude()));
                    } else {
                        Snackbar.make(rootLayout,
                                getString(R.string.get_current_location_fail),
                                Snackbar.LENGTH_LONG).show();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mSnackbar.dismiss();
                    Snackbar.make(rootLayout,
                            getString(R.string.get_current_location_fail),
                            Snackbar.LENGTH_LONG).show();
                }
            });
        } else {
            Snackbar.make(rootLayout,
                    getString(R.string.no_location_permission),
                    Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_FROM_GALLERY && resultCode == Activity.RESULT_OK) {
            Uri imageUri = data.getData();

            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            // Get the cursor
            Cursor cursor = getActivity().getContentResolver().query(imageUri, filePathColumn,
                    null, null, null);
            // Move to first row
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            mSelectedProfileImage = cursor.getString(columnIndex);
            cursor.close();

            Glide.with(getContext().getApplicationContext())
                    .load(mSelectedProfileImage)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.no_image))
                    .into(mDiveSpotProfileIv);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    class ProfileImageResultReceiver extends ResultReceiver {

        private DatabaseReference mDiveSpotReference;

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public ProfileImageResultReceiver(Handler handler) {
            super(handler);
        }

        public void setDiveSpotReference(DatabaseReference reference) {
            mDiveSpotReference = reference;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            mSnackbar.dismiss();
            if (Activity.RESULT_OK == resultCode) {
                mDbutils.setProfileImage(mDiveSpotReference,
                        resultData.getString(getString(R.string.image_uri_key)));

            } else {
                Snackbar.make(rootLayout, getString(R.string.upload_profile_image_failure),
                        Snackbar.LENGTH_LONG).show();
            }
            mListener.onFragmentInteraction(OnFragmentInteractionListener.DIVE_SPOT_ADDED, null);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(getString(R.string.dive_spot_name_key),
                mDiveSpotNameEt.getText().toString());
        outState.putString(getString(R.string.dive_spot_country_key),
                mDiveSpotCountryEt.getText().toString());
        outState.putString(getString(R.string.dive_spot_state_key),
                mDiveSpotStateEt.getText().toString());
        outState.putString(getString(R.string.dive_spot_city_key),
                mDiveSpotCityEt.getText().toString());
        outState.putString(getString(R.string.dive_spot_latitude_key),
                mDiveSpotLatitudeEt.getText().toString());
        outState.putString(getString(R.string.dive_spot_longitude_key),
                mDiveSpotLongitudeEt.getText().toString());
        outState.putString(getString(R.string.dive_spot_description_key),
                mDiveSpotDescriptionEt.getText().toString());

        if (null != mSelectedProfileImage) {
            outState.putString(getString(R.string.dive_spot_profile_image_key),
                    mSelectedProfileImage);
        }
        super.onSaveInstanceState(outState);
    }
}
