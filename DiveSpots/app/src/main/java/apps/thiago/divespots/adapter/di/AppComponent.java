package apps.thiago.divespots.adapter.di;

import dagger.Component;
import apps.thiago.divespots.DiveSpotsWidget;
import apps.thiago.divespots.MainActivity;

@Component
public interface AppComponent {

    void inject (MainActivity mainActivity);
    void inject (DiveSpotsWidget widget);
}
