package apps.thiago.divespots.data;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.security.SecureRandom;

import apps.thiago.divespots.R;

/**
 * Created by thiagom on 2/20/18.
 */

public class FirebaseStorageUtilsService extends IntentService {

    private FirebaseStorage mStorage;
    private StorageReference mReference;
    private ResultReceiver mResultReceiver;

    public FirebaseStorageUtilsService() {
        super(FirebaseStorageUtilsService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        final String imagePath = intent.getStringExtra(getString(R.string.image_path_key));
        String storagePath = intent.getStringExtra(getString(R.string.storage_path_key));
        mResultReceiver = intent.getParcelableExtra(getString(R.string.result_receiver_key));
        final String diveSpotId = intent.getStringExtra(getString(R.string.dive_spot_id_key));
        final String diveSpotName = intent.getStringExtra(getString(R.string.dive_spot_name_key));

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        mStorage = FirebaseStorage.getInstance();
        mReference = mStorage.getReference();

        /* Proposal to unique image name, to avoid conflict when using the same name, is to append
         * user's id, with image name and a random value
         */
        SecureRandom random = new SecureRandom();
        byte[] seed = random.generateSeed(10);
        final String imageName = imagePath.substring(imagePath.lastIndexOf('/')+1);

        String path = currentUser.getUid()
                .concat(String.valueOf(random.nextLong()))
                .concat(imageName);


        path = storagePath + path;

        StorageReference imgRef =
                mReference.child(path);

        UploadTask uploadTask = imgRef.putFile(Uri.fromFile(new File(imagePath)));

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //mListener.onUploadFailed(imagePath);
                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.images_path), imageName);

                mResultReceiver.send(Activity.RESULT_CANCELED, bundle);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri uploadedUri = taskSnapshot.getDownloadUrl();

                Bundle bundle = new Bundle();
                bundle.putString(getString(R.string.images_path), imageName);
                bundle.putString(getString(R.string.dive_spot_id_key), diveSpotId);
                bundle.putString(getString(R.string.image_uri_key), uploadedUri.toString());
                bundle.putString(getString(R.string.dive_spot_name_key), diveSpotName);

                mResultReceiver.send(Activity.RESULT_OK, bundle);
            }
        });
    }
}
