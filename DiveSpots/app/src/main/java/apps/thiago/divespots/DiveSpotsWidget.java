package apps.thiago.divespots;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.RemoteViews;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.AppWidgetTarget;

import java.util.List;
import java.util.Random;

import apps.thiago.divespots.data.DiveSpotWidgetImageInfo;
import apps.thiago.divespots.data.FirebaseDBUtils;

/**
 * Implementation of App Widget functionality.
 */
public class DiveSpotsWidget extends AppWidgetProvider {

    private static final String TAG = DiveSpotsWidget.class.getSimpleName();

    private static FirebaseDBUtils mDBUtils;

    static void updateAppWidget(final Context context, final AppWidgetManager appWidgetManager,
                                final int appWidgetId) {

        mDBUtils = FirebaseDBUtils.getInstance(context);
        List<DiveSpotWidgetImageInfo> images = mDBUtils.getAllImages();

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.dive_spots_widget);

        AppWidgetTarget mAppWidgetTarget = new AppWidgetTarget(context,
                                                    R.id.widget_dive_spot_image,
                                                    views,
                                                    appWidgetId);

        Random r = new Random();
        if (null != images && images.size() > 0) {
            int position = r.nextInt(images.size());

            views.setTextViewText(R.id.widget_dive_spot_name, images.get(position).getDiveSpotName());

            Glide.with(context.getApplicationContext())
                    .asBitmap()
                    .load(images.get(position).getDiveSpotImage())
                    .into(mAppWidgetTarget);
        } else {

            views.setTextViewText(R.id.widget_dive_spot_name, context.getString(R.string.loading_images));

            Log.d(TAG, "Images not retrieved yet. Will try again in 5 minutes");
            // In case no image is found, try again after 5 minutes.
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateAppWidget(context, appWidgetManager, appWidgetId);
                }
            }, 300000);
        }
        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

