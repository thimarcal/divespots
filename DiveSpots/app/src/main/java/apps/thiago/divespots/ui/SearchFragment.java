package apps.thiago.divespots.ui;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import apps.thiago.divespots.R;
import apps.thiago.divespots.data.DiveSpot;
import apps.thiago.divespots.data.FirebaseDBUtils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class SearchFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    @BindView(R.id.country_spinner)
    Spinner mCountrySpinner;

    @BindView(R.id.state_spinner)
    Spinner mStateSpinner;

    @BindView(R.id.city_spinner)
    Spinner mCitySpinner;

    @BindView(R.id.search_button)
    Button mSearchButton;

    private OnFragmentInteractionListener mListener;

    private FirebaseDBUtils mDbUtils;

    // Lists retrieved from DB
    private List<String> mCountries;
    private List<String> mStates;
    private List<String> mCities;

    private List<DiveSpot> mDiveSpots;

    // Selections
    private String mSelectedCountry;
    private String mSelectedState;
    private String mSelectedCity;

    private boolean instanceSaved = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        ButterKnife.bind(this, view);

        mDbUtils = FirebaseDBUtils.getInstance(getActivity().getApplicationContext());

        if (null == savedInstanceState) {
            loadCountries();
        } else {
            instanceSaved = true;

            mCountries = savedInstanceState.getStringArrayList(getString(R.string.country_list_key));
            mStates = savedInstanceState.getStringArrayList(getString(R.string.state_list_key));
            mCities = savedInstanceState.getStringArrayList(getString(R.string.city_list_key));

            int countryPosition = savedInstanceState.getInt(
                    getString(R.string.selected_country_key));

            int statePosition = savedInstanceState.getInt(
                    getString(R.string.selected_state_key));

            int cityPosition = savedInstanceState.getInt(
                    getString(R.string.selected_city_key));

            mSelectedCountry = mCountries.get(countryPosition);
            mSelectedState = mStates.get(statePosition);
            mSelectedCity = mCities.get(cityPosition);

            // Set Adapters to retrieved data
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, mCountries);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mCountrySpinner.setAdapter(dataAdapter);
            mCountrySpinner.setSelection(countryPosition, false);


            dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, mStates);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mStateSpinner.setAdapter(dataAdapter);
            mStateSpinner.setSelection(statePosition, false);


            dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, mCities);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mCitySpinner.setAdapter(dataAdapter);
            mCitySpinner.setSelection(cityPosition, false);

        }
        mCountrySpinner.setOnItemSelectedListener(this);
        mStateSpinner.setOnItemSelectedListener(this);
        mCitySpinner.setOnItemSelectedListener(this);
        mSearchButton.setOnClickListener(this);

        setRetainInstance(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (null != savedInstanceState) {
            instanceSaved = true;

            mCountries = savedInstanceState.getStringArrayList(getString(R.string.country_list_key));
            mStates = savedInstanceState.getStringArrayList(getString(R.string.state_list_key));
            mCities = savedInstanceState.getStringArrayList(getString(R.string.city_list_key));

            int countryPosition = savedInstanceState.getInt(
                    getString(R.string.selected_country_key));

            int statePosition = savedInstanceState.getInt(
                    getString(R.string.selected_state_key));

            int cityPosition = savedInstanceState.getInt(
                    getString(R.string.selected_city_key));

            mSelectedCountry = mCountries.get(countryPosition);
            mSelectedState = mStates.get(statePosition);
            mSelectedCity = mCities.get(cityPosition);

            // Set Adapters to retrieved data
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, mCountries);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mCountrySpinner.setAdapter(dataAdapter);
            mCountrySpinner.setSelection(countryPosition, false);


            dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, mStates);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mStateSpinner.setAdapter(dataAdapter);
            mStateSpinner.setSelection(statePosition, false);


            dataAdapter = new ArrayAdapter<String>(getContext(),
                    android.R.layout.simple_spinner_item, mCities);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mCitySpinner.setAdapter(dataAdapter);
            mCitySpinner.setSelection(cityPosition, false);
        }
    }

    /**
     * This auxiliary method is used to set previous selected items, specially when returning from
     * "List" view
     *
     * @param country
     * @param state
     * @param city
     */
    public void setSelections(int country, int state, int city) {
        loadStates(mCountries.get(country));
        loadCities(mCountries.get(country), mStates.get(state));
        instanceSaved = true;
        mCountrySpinner.setSelection(country);
        mStateSpinner.setSelection(state);
        mCitySpinner.setSelection(city);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void dataUpdated() {
        String currentCountry = mSelectedCountry;
        String currentState = mSelectedState;
        String currentCity = mSelectedCity;

        loadCountries();
        mCountrySpinner.setSelection(mCountries.indexOf(currentCountry), false);
        loadStates(currentCountry);
        mStateSpinner.setSelection(mStates.indexOf(currentState), false);
        loadCities(currentCountry, currentState);
        mCitySpinner.setSelection(mCities.indexOf(currentCity), false);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.country_spinner:
                if (!instanceSaved &&
                        (null == mSelectedCountry || !mSelectedCountry.equals(mCountries.get(position)))) {
                    // Loads States when country is selected
                    mSelectedCountry = mCountries.get(position);
                    loadStates(mSelectedCountry);
                }
                break;
            case R.id.state_spinner:
                if (!instanceSaved &&
                        (null == mSelectedState || !mSelectedState.equals(mStates.get(position)))) {
                    mSelectedState = mStates.get(position);
                    loadCities(mSelectedCountry, mSelectedState);
                }
                break;
            case R.id.city_spinner:
                if (!instanceSaved &&
                        (null == mSelectedCity || !mSelectedCity.equals(mStates.get(position)))) {
                    mSelectedCity = mCities.get(position);
                }
                instanceSaved = false;
                break;

            default:
                //Should never reach here
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /**
     * Retrieves Countries from DB and loads them for selection
     */
    private void loadCountries() {
        if (null == mCountrySpinner) {
            return;
        }
        mCountries = mDbUtils.getCountries();

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, mCountries);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCountrySpinner.setAdapter(dataAdapter);

    }

    /**
     * Loads States from selected Country
     *
     * @param country
     */
    private void loadStates(String country) {
        if (null == mStateSpinner) {
            return;
        }
        mStates = mDbUtils.getStates(country);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, mStates);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStateSpinner.setAdapter(dataAdapter);
    }

    private void loadCities(String country, String state) {
        if (null == mCitySpinner) {
            return;
        }
        mCities = mDbUtils.getCities(country, state);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, mCities);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCitySpinner.setAdapter(dataAdapter);
    }

    @Override
    public void onClick(View v) {
        mDiveSpots = mDbUtils.getDiveSpots(mSelectedCountry, mSelectedState, mSelectedCity);

        // Send information to mainActivity, to display dive spots in another fragment
        Bundle bundle = new Bundle();
        bundle.putInt(getString(R.string.selected_country_key), mCountrySpinner.getSelectedItemPosition());
        bundle.putInt(getString(R.string.selected_state_key), mStateSpinner.getSelectedItemPosition());
        bundle.putInt(getString(R.string.selected_city_key), mCitySpinner.getSelectedItemPosition());
        bundle.putParcelableArrayList(getString(R.string.dive_spots_key),
                                    (ArrayList<DiveSpot>)mDiveSpots);
        mListener.onFragmentInteraction(OnFragmentInteractionListener.RESULTS_LOADED, bundle);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putStringArrayList(getString(R.string.country_list_key), (ArrayList<String>)mCountries);
        outState.putStringArrayList(getString(R.string.state_list_key), (ArrayList<String>)mStates);
        outState.putStringArrayList(getString(R.string.city_list_key), (ArrayList<String>)mCities);

        outState.putInt(getString(R.string.selected_country_key), mCountrySpinner.getSelectedItemPosition());
        outState.putInt(getString(R.string.selected_state_key), mStateSpinner.getSelectedItemPosition());
        outState.putInt(getString(R.string.selected_city_key), mCitySpinner.getSelectedItemPosition());
        super.onSaveInstanceState(outState);
    }
}
